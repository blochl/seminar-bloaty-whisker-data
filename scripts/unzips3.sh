# Function to process JSON files in the current directory and its subdirectories
process_json_files() {
    for file in "$1"/*; do
        if [[ -d "$file" ]]; then
            # If it's a directory, recursively call the function
            process_json_files "$file"
        elif [[ -f "$file" && "${file##*.}" == "sb3" ]]; then
            # If it's a JSON file, run the specified command on it
            echo "Processing: $file"
            parent_dir=$(dirname "$file")
            filename=$(basename "$file")
            filename_no_extension="${filename%.*}"
            echo "par dir: $parent_dir"
            echo "filename: $filename"
            
            unzip -j "$file" project.json -d ./"$parent_dir"/"$filename_no_extension"

            echo "--------------------------------------"
        fi
    done
}

# Call the function starting from the current directory
process_json_files "."