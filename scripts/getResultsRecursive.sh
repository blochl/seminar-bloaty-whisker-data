# Function to process JSON files in the current directory and its subdirectories
process_json_files() {
    for file in "$1"/*; do
        if [[ -d "$file" ]]; then
            # If it's a directory, recursively call the function
            process_json_files "$file"
        elif [[ -f "$file" && "${file##*.}" == "json" ]]; then
            # If it's a JSON file, run the specified command on it
            echo "Processing: $file"
            parent_dir=$(dirname "$file")

            echo "par dir: $parent_dir"

            # New file name
            new_name="${parent_dir}.json"

            cp "$file" "$new_name";

            java -jar Litterbox-1.9-SNAPSHOT.jar stats --path "$new_name" --output stats.csv
            echo "--------------------------------------"
        fi
    done
}

# Call the function starting from the current directory
process_json_files "."