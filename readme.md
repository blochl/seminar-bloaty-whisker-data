## Structure of this repo:

### Repair Subjects:
- This folder contains all original Scratch programs used for the study, all games from data set 1 as well as the Fruit Catching projects.

### Result Projects:
- This folder has subfolders for data set 1 and 2 for bloaty and bloat-free repair results. 
- In the data set 1 folders there are separate folders for each game, containing the solutions from all 15 runs (if there was a solution found).
- The suffix number tells the repetition number.
- In the data set 2 folders there are separate folders for each game, containing the best variant from all 10 runs (only in json format).
- The suffix of the file again tells the repetition number.

### Scripts:
- Just some scripts used to extract data and use e.g. Litterbox.

### Some Plots:
- It holds a markdown file with some images and plots with the experiment results (some are already in the seminar paper, some not).

### Statistics:
- This folder contains the output of Litterbox used to extract the number of blocks from each solution/variant (but it gives more information as well, e.g. the kinds of blocks,..)

### Tests
- Those are the tests used for the Whisker repair runs for each of the Scratch games.