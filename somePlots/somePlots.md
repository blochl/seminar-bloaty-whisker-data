## Data Set 1:

### Number of Repaired Programs:
![Alt text](image-5.png)
![Alt text](image-6.png)

### Run time:
![Alt text](image-3.png)
![Alt text](image-4.png)
### Solution Size (Number of blocks):
![Alt text](image-2.png)
![Alt text](image-7.png)



## Data Set 2:

### Number of Passed Tests Of Best Variant:
![Alt text](image-10.png)
![Alt text](image-14.png)

### Run time:
![Alt text](image-9.png)

### Variant Size (Number of blocks):
![Alt text](image-1.png)
![Alt text](image-8.png)